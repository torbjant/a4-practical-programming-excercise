package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
@Nested
class PlayingCardTest {

  private PlayingCard testPlayingCard;

  @BeforeEach
  public void setUp() {
    this.testPlayingCard = new PlayingCard('C', 1);
  }

  @Test
  public void getSuitTest() {
    Assertions.assertEquals('C', testPlayingCard.getSuit());
  }

  @Test
  public void getFaceTest() {
    Assertions.assertEquals(1, testPlayingCard.getFace());
  }

  @Test
  public void getAsStringTest() {
    Assertions.assertEquals("C1", testPlayingCard.getAsString());
  }

  @Test
  public void getImagePathTest() {
    Assertions.assertEquals("cardImages/01C.png", testPlayingCard.getImagePath());
  }


}