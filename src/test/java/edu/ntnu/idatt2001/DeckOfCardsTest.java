package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@Nested
class DeckOfCardsTest {

  @Test
  public void doesCardsHaveRightValue()  {
    DeckOfCards doc = new DeckOfCards();
    List<PlayingCard> deckOfCards = doc.getDeckOfCards();
    assertEquals("C1", deckOfCards.get(0).getAsString());
    assertNotEquals("C2", deckOfCards.get(0).getAsString());
    assertEquals("S13", deckOfCards.get(51).getAsString());
  }

  @Test
  public void dealHandDealsRightAmountOfCards() {
    DeckOfCards doc = new DeckOfCards();
    HandOfCards hoc = new HandOfCards();
    doc.dealHand(hoc,5);

    assertEquals(5, hoc.getHand().size());
  }

}