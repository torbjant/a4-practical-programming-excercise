package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@Nested
class HandOfCardsTest {

  private HandOfCards testHandOfCards;

  @BeforeEach
  public void setUp() {
    this.testHandOfCards = new HandOfCards();
    List<PlayingCard> hand = new ArrayList<>();
    PlayingCard playingCard;
    for (int i = 0; i < 5; i++) {
      playingCard = new PlayingCard('C', 1);
      hand.add(playingCard);
    }
    testHandOfCards.setHand(hand);
  }

  @Test
  public void getSumOfFacesTest() {
    Assertions.assertNotNull(testHandOfCards.getHand());
    Assertions.assertEquals(5, testHandOfCards.getSumOfFaces());
  }

  @Test
  public void getCardsOfHeartsNegativeTest() {
    Assertions.assertNotNull(testHandOfCards.getHand());
    Assertions.assertEquals(0, testHandOfCards.getCardsOfHearts().size());
  }

  @Test
  public void getCardsOfHeartsPositiveTest() {
    List<PlayingCard> hand = new ArrayList<>();
    PlayingCard playingCard;
    for (int i = 0; i < 5; i++) {
      playingCard = new PlayingCard('H', 1);
      hand.add(playingCard);
    }
    testHandOfCards.setHand(hand);
    Assertions.assertEquals(5, testHandOfCards.getCardsOfHearts().size());
  }

  @Test
  public void anyQueenOfSpadesNegativeTest() {
    Assertions.assertFalse(testHandOfCards.anyQueenOfSpades());
  }

  @Test
  public void anyQueenOfSpadesPositiveTest() {
    List<PlayingCard> hand = new ArrayList<>();
    PlayingCard playingCard;
    for (int i = 0; i < 5; i++) {
      playingCard = new PlayingCard('S', 12);
      hand.add(playingCard);
    }
    testHandOfCards.setHand(hand);
    Assertions.assertTrue(testHandOfCards.anyQueenOfSpades());
  }

  @Test
  public void isFlushPositiveTest() {
    Assertions.assertTrue( testHandOfCards.isFlush());
  }

  @Test
  public void isFlushNegativeTest() {
    List<PlayingCard> hand = new ArrayList<>();
    PlayingCard playingCard;
    for (int i = 0; i < 4; i++) {
      playingCard = new PlayingCard('S', 12);
      hand.add(playingCard);
    }
    playingCard = new PlayingCard('D', 12);
    hand.add(playingCard);
    testHandOfCards.setHand(hand);
    Assertions.assertFalse(testHandOfCards.isFlush());
  }
}