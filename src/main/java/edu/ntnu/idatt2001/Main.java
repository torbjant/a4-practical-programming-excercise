package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;


public class Main extends Application {
  private final Color DARKGRAY = Color.rgb(54, 69, 79).deriveColor(
      0.0, 1.0, 0.4, 1.0);

  private Controller controller;
  private HandOfCards handOfCards;
  private ArrayList<ImageView> imageViews;
  private ArrayList<TextField> textFields;


  public static void main(String[] args)  {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.imageViews = new ArrayList<>();
    this.textFields = new ArrayList<>();
    this.controller = new Controller(this);
    this.handOfCards = new HandOfCards();
    Text handText = new Text("Hand");
    DeckOfCards deckOfCards = new DeckOfCards();

    GridPane root = new GridPane();
    root.setPrefSize(1280, 800);
    root.setPadding(new Insets(10, 10, 10, 10));
    root.setHgap(20);
    root.setVgap(20);
    root.setBackground(new Background(new BackgroundFill(DARKGRAY,
        new CornerRadii(0), new Insets(0))));
    root.setAlignment(Pos.CENTER);

    BorderPane hand = new BorderPane(handText);

    StackPane handTitle = new StackPane(handText);
    handTitle.setBackground(new Background(new BackgroundFill(Color.GRAY,
        new CornerRadii(0), new Insets(0))));
    handTitle.setPrefHeight(20);
    GridPane handImages = createHandImageViews(5);
    hand.setTop(handTitle);
    hand.setCenter(handImages);
    root.add(hand, 0,0);
    root.add(createTextsAndTextFields(), 0, 1);
    root.add(createButtons(), 1, 0);

    Scene scene = new Scene(root,1280, 800, DARKGRAY);

    primaryStage.setTitle("CardGame");
    primaryStage.setScene(scene);
    primaryStage.setResizable(false);

    ImageView imageView = new ImageView();

    Image icon = new Image("card_icon.png");
    imageView.setImage(icon);
    primaryStage.getIcons().add(icon);
    primaryStage.show();
  }

  public VBox createButtons() {
    Button dealHand = new Button("Deal hand");
    DeckOfCards deckOfCards = new DeckOfCards();
    dealHand.setPrefSize(100,40);
    dealHand.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        controller.doDrawHand(handOfCards);
      }
    });
    Button checkHand = new Button("Check hand");
    checkHand.setPrefSize(100,40);
    checkHand.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {

        controller.doCheckHand(handOfCards);
      }
    });
    VBox vBox = new VBox(dealHand, checkHand);
    vBox.setSpacing(20);
    vBox.setAlignment(Pos.CENTER);

    return vBox;
  }

  public GridPane createTextsAndTextFields()  {
    GridPane gridPane = new GridPane();
    gridPane.setHgap(5);
    gridPane.setVgap(5);

    ArrayList<Text> texts = new ArrayList<>();
    ArrayList<HBox> hBoxes = new ArrayList<>();

    texts.add(new Text("Sum of the faces: "));
    texts.add(new Text("Cards of hearts: "));
    texts.add(new Text("Flush: "));
    texts.add(new Text("Queen of spades: "));

    textFields.add(new TextField());

    for (int i = 0; i < 4; i++) {
      texts.get(i).setFill(Color.WHITE);
      textFields.add(new TextField());
      textFields.get(i).setEditable(false);
      hBoxes.add(new HBox(texts.get(i), textFields.get(i)));
      hBoxes.get(i).setAlignment(Pos.CENTER);
      if (i < 2)  {
        gridPane.add(hBoxes.get(i), i, 0);
      } else {
        gridPane.add(hBoxes.get(i), (i - 2), 1);
      }
    }


    gridPane.setAlignment(Pos.CENTER);
    return gridPane;
  }
  public void setHandInImageView(HandOfCards hand)  {
    int i = 0;
    Image image;
    for (PlayingCard playingCard : hand.getHand())  {
      image = new Image(playingCard.getImagePath());
      this.imageViews.get(i).setImage(image);
      i++;
    }
  }
  public void setHandData(HandOfCards hand) {
    this.textFields.get(0).setText(Integer.toString(hand.getSumOfFaces()));
    this.textFields.get(1).setText(hand.cardsToString(hand.getCardsOfHearts()));
    this.textFields.get(2).setText(Boolean.toString(hand.isFlush()));
    this.textFields.get(3).setText(Boolean.toString(hand.anyQueenOfSpades()));

  }
  public GridPane createHandImageViews(int n)  {
    GridPane gridPane = new GridPane();
    for (int i = 0; i < n; i++) {
      ImageView imageView = new ImageView();
      imageView.setFitHeight(100);
      imageView.setPreserveRatio(true);
      this.imageViews.add(imageView);
      gridPane.add(imageViews.get(i), i, 0);
    }
    gridPane.setAlignment(Pos.CENTER);
    gridPane.setHgap(5);
    gridPane.setVgap(5);
    gridPane.setBackground(new Background(new BackgroundFill(Color.GRAY,
        new CornerRadii(0), new Insets(0))));

    return gridPane;
  }

  @Override
  public void init() throws Exception {
    super.init();
  }

  @Override
  public void stop() throws Exception{
    super.stop();
  }
}