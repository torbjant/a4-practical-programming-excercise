package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
  private final List<PlayingCard> deckOfCards;

  public DeckOfCards()  {
    this.deckOfCards = new ArrayList<>();
    char[] suit = {'C', 'D', 'H', 'S'};
    for (char c : suit) {
      for (int j = 0; j <= 12; j++) {
        deckOfCards.add(new PlayingCard(c, j+1));
      }
    }
  }


  public void dealHand(HandOfCards handOfCards, int n) {

    List<PlayingCard> drawDeck = deckOfCards;
    List<PlayingCard> hand = new ArrayList<>();
    Random random = new Random();

    int i;

    for (int j = 0; j < n; j++) {
      i = random.nextInt(52-j);
      hand.add(drawDeck.get(i));
      drawDeck.remove(i);
    }
    handOfCards.setHand(hand);
  }

  public List<PlayingCard> getDeckOfCards() {
    return deckOfCards;
  }
}