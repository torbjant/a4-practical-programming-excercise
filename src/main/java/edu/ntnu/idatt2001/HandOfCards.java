package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;

public class HandOfCards {

  private List<PlayingCard> hand;

  public HandOfCards()  {
    this.hand = new ArrayList<>();
  }

  public List<PlayingCard> getHand()  {
    return this.hand;
  }

  public void setHand(List<PlayingCard> hand) {
    this.hand = hand;
  }
  public int getSumOfFaces()  {
    return this.hand.stream()
        .map(PlayingCard::getFace)
        .reduce(Integer::sum)
        .orElse(-1);
  }

  public List<PlayingCard> getCardsOfHearts() {
    return this.hand.stream()
        .filter(p -> 'H' == p.getSuit())
        .toList();
  }

  public boolean anyQueenOfSpades() {
    return hand.stream()
        .anyMatch(p -> 'S' == p.getSuit() && 12 == p.getFace());
  }

  public boolean isFlush()  {
    return hand.stream()
        .allMatch(p -> 'S' == p.getSuit())
        || hand.stream()
        .allMatch(p -> 'H' == p.getSuit())
        || hand.stream()
        .allMatch(p -> 'D' == p.getSuit())
        || hand.stream()
        .allMatch(p -> 'C' == p.getSuit());
  }
  public String cardsToString(List<PlayingCard> cards)  {
    StringBuilder str = new StringBuilder();
    if (cards.size() != 0)  {
      for (PlayingCard card : cards) {
        str.append(card.getAsString()).append(" ");
      }
    } else {
      str.append("No hearts in hand");
    }
    return str.toString();
  }
}
