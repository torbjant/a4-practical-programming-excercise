package edu.ntnu.idatt2001;

public class Controller {
  private final Main view;

  public Controller(Main view)  {
    this.view = view;
  }

  public void doDrawHand(HandOfCards hand)  {
    DeckOfCards deckOfCards = new DeckOfCards();
    deckOfCards.dealHand(hand, 5);
    this.view.setHandInImageView(hand);
  }

  public void doCheckHand(HandOfCards hand) {
    this.view.setHandData(hand);
  }
}
